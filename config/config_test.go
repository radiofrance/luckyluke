package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigSimple(t *testing.T) {
	conf, err := LoadConfig("fixtures/config-simple.yml")
	if err != nil {
		t.Fatalf("Error loading config: %v", err)
	}
	bounties := conf.GetBounties()
	assert.Len(t, bounties, 2)
	assert.EqualValues(t, bounties["b1"], Bounty{
		Namespace:    "a",
		Context:      "c",
		Deployment:   "d",
		MinAvailable: 2,
		MinAge:       "2h",
		Cron:         "TZ=Europe/Paris */10 * * * * *",
	})
	assert.EqualValues(t, bounties["b2"], Bounty{
		Namespace:    "a2",
		Context:      "c2",
		Deployment:   "d2",
		MinAvailable: 3,
		Cron:         "TZ=Europe/London */10 * * * * *",
	})
}

func TestConfigGlobal(t *testing.T) {
	conf, err := LoadConfig("fixtures/config-global.yml")
	if err != nil {
		t.Fatalf("Error loading config: %v", err)
	}
	bounties := conf.GetBounties()
	assert.Len(t, bounties, 2)
	assert.EqualValues(t, bounties["b1"], Bounty{
		Namespace:    "globaln",
		Context:      "c",
		Deployment:   "d",
		MinAvailable: 2,
		Cron:         "TZ=Europe/Paris */10 * * * * *",
	})
	assert.EqualValues(t, bounties["b2"], Bounty{
		Namespace:    "a2",
		Context:      "globalc",
		Deployment:   "d2",
		MinAvailable: 3,
		Cron:         "TZ=Europe/London */10 * * * * *",
	})
}
