package config

import (
	"fmt"
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

func LoadConfig(path string) (*Config, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("failed to read file %s: %v", path, err)
	}
	c := Config{}
	err = yaml.Unmarshal(data, &c)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal file %s: %v", path, err)
	}
	return &c, nil
}

type Config struct {
	LogLevel  string            `yaml:"log_level"`
	LogFormat string            `yaml:"log_format"`
	Global    *Bounty           `yaml:"global"`
	Bounties  map[string]Bounty `yaml:"bounties"`
}

func (c *Config) GetBounties() map[string]Bounty {
	final := map[string]Bounty{}
	for bountyName, bounty := range c.Bounties {
		if c.Global == nil {
			final[bountyName] = bounty
			continue
		}
		new := Bounty{
			Context:                       c.Global.Context,
			Namespace:                     c.Global.Namespace,
			Deployment:                    c.Global.Deployment,
			MinAvailable:                  c.Global.MinAvailable,
			MinAge:                        c.Global.MinAge,
			TerminationGracePeriodSeconds: c.Global.TerminationGracePeriodSeconds,
			Cron:                          c.Global.Cron,
		}
		if len(bounty.Context) > 0 {
			new.Context = bounty.Context
		}
		if len(bounty.Namespace) > 0 {
			new.Namespace = bounty.Namespace
		}
		if len(bounty.Deployment) > 0 {
			new.Deployment = bounty.Deployment
		}
		if bounty.MinAvailable != 0 {
			new.MinAvailable = bounty.MinAvailable
		}
		if len(bounty.MinAge) > 0 {
			new.MinAge = bounty.MinAge
		}
		if bounty.TerminationGracePeriodSeconds != nil {
			new.TerminationGracePeriodSeconds = bounty.TerminationGracePeriodSeconds
		}
		if len(bounty.Cron) > 0 {
			new.Cron = bounty.Cron
		}
		final[bountyName] = new
	}
	return final
}

type Bounty struct {
	Context                       string `yaml:"context"`
	Namespace                     string `yaml:"namespace"`
	Deployment                    string `yaml:"deployment"`
	MinAvailable                  int    `yaml:"minAvailable"`
	MinAge                        string `yaml:"minAge"`
	TerminationGracePeriodSeconds *int64 `yaml:"terminationGracePeriodSeconds"`
	Cron                          string `yaml:"cron"`
}
