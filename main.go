package main

import (
	"fmt"
	"os"

	"gitlab.com/radiofrance/luckyluke/config"
	"gitlab.com/radiofrance/luckyluke/telegraph"

	cli "github.com/jawher/mow.cli"
	log "github.com/sirupsen/logrus"
)

const (
	version = "v0.0"
)

func main() {
	app := cli.App("luke", "Kill Pods faster than his own shadow")

	app.Command("schedule", "Start lucky luke on a schedule", cmdSchedule)
	app.Command("-v --version version", "Display lucky luke version", cmdGetVersion)

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func cmdSchedule(cmd *cli.Cmd) {
	configPath := cmd.StringOpt("config", "/etc/luckyluke/config.yml", "Lucky Luke config path")
	dryRun := cmd.BoolOpt("dry-run", false, "Dry-run mode, no pod will be harmed")
	cmd.Action = func() {
		conf, err := config.LoadConfig(*configPath)
		if err != nil {
			log.Fatalf("Could not read configuration: %v", err)
		}
		err = telegraph.ConfigureLogger(log.StandardLogger(), conf)
		if err != nil {
			log.Fatalf("Failed to configure Telegraph: %v", err)
		}
		startSchedule(conf.GetBounties(), *dryRun)
	}
}

func cmdGetVersion(cmd *cli.Cmd) {
	cmd.Action = func() {
		fmt.Printf("%s\n", version)
	}
}
