FROM debian:stretch

RUN set -x \
    && apt-get update -y \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*

COPY luckyluke /usr/local/bin/luckyluke

RUN chmod +x /usr/local/bin/luckyluke

ENTRYPOINT [ "/usr/local/bin/luckyluke" ]
