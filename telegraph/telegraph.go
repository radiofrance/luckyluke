package telegraph

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/radiofrance/luckyluke/config"
)

// ConfigureLogger will set the LogLevel, LogFormat and Output to the given logger,
// according to the configuration given
func ConfigureLogger(log *logrus.Logger, c *config.Config) error {
	switch c.LogFormat {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	case "text", "":
		logrus.SetFormatter(&logrus.TextFormatter{})
	default:
		return fmt.Errorf("log_format configuration option should be one of [text, json], actual: %s", c.LogFormat)
	}

	if len(c.LogLevel) == 0 {
		logrus.SetLevel(logrus.InfoLevel)
	} else {
		parsed, err := logrus.ParseLevel(c.LogLevel)
		if err != nil {
			return err
		}
		logrus.SetLevel(parsed)
	}
	logrus.SetOutput(os.Stdout)
	return nil
}
