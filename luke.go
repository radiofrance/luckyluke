package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/radiofrance/kubecli"
	"gitlab.com/radiofrance/luckyluke/config"
	"gitlab.com/radiofrance/luckyluke/sheriff"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func startSchedule(bounties map[string]config.Bounty, dryRun bool) {
	log.Infof("Lucky luke is starting")
	if len(bounties) > 0 {
		c := cron.New()
		for bountyName, bounty := range bounties {
			_, err := c.AddJob(bounty.Cron, Killer{Bounty: bounty, DryRun: dryRun})
			if err != nil {
				log.Fatalf("Could not add cron expression: %v", err)
			}
			log.Infof("Started Bounty %s with schedule: %s", bountyName, bounty.Cron)
		}
		c.Start()
		select {}
	} else {
		log.Infof("No bounty found in config, I'll keep being a poor lonesome cowboy")
	}
}

type Killer struct {
	Bounty config.Bounty
	DryRun bool
}

func (k Killer) Run() {
	if err := kill(k.Bounty, k.DryRun); err != nil {
		log.Errorf("Could not kill pod: %v", err)
	}
}

func kill(bounty config.Bounty, dryRun bool) error {
	kube, err := kubecli.New(bounty.Context)
	if err != nil {
		return fmt.Errorf("could not create kubecli: %v", err)
	}

	deployment, err := kube.ClientSet.AppsV1().Deployments(bounty.Namespace).Get(
		context.Background(), bounty.Deployment, metav1.GetOptions{})
	if err != nil {
		return fmt.Errorf("could not get Deployment: %v", err)
	}

	replicaSet, err := kube.GetReplicaSet(deployment)
	if err != nil {
		return fmt.Errorf("could not get ReplicaSet: %v", err)
	}

	pods, err := kube.FindPods(replicaSet)
	if err != nil {
		return fmt.Errorf("could not find Pods: %v", err)
	}

	killList, err := sheriff.KillList(bounty, pods)
	if err != nil {
		log.Infof("Kill not authorized: %v", err)
		return nil
	}

	victim := pickVictim(killList)
	log.WithFields(log.Fields{
		"context":   bounty.Context,
		"namespace": bounty.Namespace,
		"pod":       victim.GetName(),
	}).Info("Found Victim")

	if dryRun {
		log.WithFields(log.Fields{
			"context":   bounty.Context,
			"namespace": bounty.Namespace,
			"pod":       victim.GetName(),
		}).Warn("[dry-run on] Victim would have been killed")
	} else {
		deleteOptions := metav1.DeleteOptions{}
		if bounty.TerminationGracePeriodSeconds != nil {
			deleteOptions.GracePeriodSeconds = bounty.TerminationGracePeriodSeconds
		}

		err = kube.ClientSet.CoreV1().Pods(victim.GetNamespace()).Delete(
			context.Background(), victim.GetName(), deleteOptions)
		if err != nil {
			return fmt.Errorf("could not delete Victim: %v", err)
		}
		log.WithFields(log.Fields{
			"context":   bounty.Context,
			"namespace": bounty.Namespace,
			"pod":       victim.GetName(),
		}).Warn("Victim killed")
	}
	return nil
}

func pickVictim(pods []corev1.Pod) corev1.Pod {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return pods[r.Intn(len(pods))]
}
