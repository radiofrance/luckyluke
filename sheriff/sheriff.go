package sheriff

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/radiofrance/luckyluke/config"
	corev1 "k8s.io/api/core/v1"
)

// KillList returns the list of pods you are authorized to kill because they match criteria
// such as minAge or readyState
func KillList(bounty config.Bounty, pods []corev1.Pod) ([]corev1.Pod, error) {
	var killList []corev1.Pod
	podReadyCount := 0
	podReadyMinAgeCount := 0
	for _, pod := range pods {
		podReady := true
		for _, containerStatus := range pod.Status.ContainerStatuses {
			podReady = podReady && containerStatus.Ready
		}

		if podReady {
			podReadyCount++
		} else {
			continue
		}

		if len(bounty.MinAge) == 0 {
			// No minAge specified
			podReadyMinAgeCount++
			killList = append(killList, pod)
			continue
		}

		minAge, err := time.ParseDuration(bounty.MinAge)
		if err != nil {
			// TODO: MinAge should be parsed during config validation
			log.Fatalf("could not parse duration %s: %v", bounty.MinAge, err)
		}
		if time.Since(pod.Status.StartTime.Time).Nanoseconds() > minAge.Nanoseconds() {
			podReadyMinAgeCount++
			killList = append(killList, pod)
		}

	}

	switch {
	case podReadyCount <= bounty.MinAvailable:
		return []corev1.Pod{}, fmt.Errorf("not enough ready pods found: %d (minAvailable: %d), skipping kill", podReadyCount, bounty.MinAvailable)
	case podReadyMinAgeCount == 0:
		return []corev1.Pod{}, fmt.Errorf("not enough ready pods with minAge found: %d (minAvailable: %d, minAge: %s), skipping kill", podReadyMinAgeCount, bounty.MinAvailable, bounty.MinAge)
	default:
		return killList, nil
	}

}
