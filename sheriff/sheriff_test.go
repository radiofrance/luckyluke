package sheriff

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/radiofrance/luckyluke/config"
	corev1 "k8s.io/api/core/v1"
)

func TestKillNotOk(t *testing.T) {
	_, err := KillList(config.Bounty{MinAvailable: 2}, twoPodsReady())
	assert.Error(t, err)
}

func TestKillOk(t *testing.T) {
	killList, err := KillList(config.Bounty{MinAvailable: 2}, threePodsReady())
	assert.NoError(t, err)
	assert.Len(t, killList, 3)
}

func threePodsReady() []corev1.Pod {
	return []corev1.Pod{
		{
			Status: corev1.PodStatus{
				ContainerStatuses: []corev1.ContainerStatus{
					{Ready: true},
					{Ready: true},
					{Ready: true},
				},
			},
		},
		{
			Status: corev1.PodStatus{
				ContainerStatuses: []corev1.ContainerStatus{
					{Ready: true},
					{Ready: true},
					{Ready: true},
				},
			},
		},
		{
			Status: corev1.PodStatus{
				ContainerStatuses: []corev1.ContainerStatus{
					{Ready: true},
					{Ready: true},
					{Ready: true},
				},
			},
		},
	}
}

func twoPodsReady() []corev1.Pod {
	return []corev1.Pod{
		{
			Status: corev1.PodStatus{
				ContainerStatuses: []corev1.ContainerStatus{
					{Ready: true},
					{Ready: true},
					{Ready: false},
				},
			},
		},
		{
			Status: corev1.PodStatus{
				ContainerStatuses: []corev1.ContainerStatus{
					{Ready: true},
					{Ready: true},
					{Ready: true},
				},
			},
		},
		{
			Status: corev1.PodStatus{
				ContainerStatuses: []corev1.ContainerStatus{
					{Ready: true},
					{Ready: true},
					{Ready: true},
				},
			},
		},
	}
}
