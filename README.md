# Lucky Luke

Lucky Luke, just a poor lonesome cowboy, has been given a mission. Find pods and kill them!

For that, you give him bounties, for each pod you want to kill.

# Bounties

A Bounty describes your target, and the schedule to kill it

```yaml
bounties:
  killnginx:
    context: prod                                 # Optional
    namespace: default
    deployment: nginx-deployment
    minAvailable: 2                               # Optional (default to 0)
    minAge: 2h                                    # Optional (for allowed values, see golang time.ParseDuration)
    cron: "TZ=Europe/Paris * */10 10-17 * * *"
    terminationGracePeriodSeconds: 0              # Optional
```

With those bounties, Lucky Luke will start his mission, and look for pods, within the deployment specified. Once it has found one (or more), it will select a random one and kill it!

# The Sheriff

The Sheriff is here to protect innoncent pods. Lucky Luke has to ask him the permission before terminating any pod.

For instance, it will prevent Luky Luke from killing a pod, if the desired number of his fellow companions are not ready (_`minAvailable` configuration_)

# Command line options

```
luckyluke -h

Usage: luke COMMAND [arg...]

Kill Pods faster than his own shadow

Commands:
  schedule                 Start lucky luke on a schedule
  -v, --version, version   Display lucky luke version
 ```

## Schedule

```
luckyluke schedule -h

Usage: luke schedule [OPTIONS]

Start lucky luke on a schedule

Options:
      --config    Lucky Luke config path (default "/etc/luckyluke/config.yml")
      --dry-run   Dry-run mode, no pod will be harmed
```

## Dry Run

You can ask Lucky Luke not to kill any pod, by adding the `--dry-run` option to the command line

# Configuration

## Global bounties configuration

If multiple bounties apply to the same targets, the `global` configuration keywork will define common attributes (for instance context, or namespace). Any particular bounty that overiddes a global configuration key will take precedence

```yaml
global:
  context: prod  # This value will be the default for all bounties, unless overridden
  namespace: default
bounties:
  killnginx:
    deployment: nginx-deployment
    minAvailable: 2                               # Optional (default to 0)
    cron: "TZ=Europe/Paris * */10 10-17 * * *"
    terminationGracePeriodSeconds: 0              # Optional
  other:
    ...
```

## Logging

Output logs can be configured through two configuration keys, `log_format` and `log_level`.

```yaml
log_format: json   # Default is text, valid values are [json, text]
log_level: warning # Default is info, valid values are [panic, fatal, error, warn, warning, info, debug, trace]
```
